<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::channel('tasks.{project}', function ($user, \App\Project $project) {
    // When just accessing a private channel, just return true or false
    // return $project->participants->contains($user);

    // When joining a channel, you need to return information about who is joining
    if($project->participants->contains($user)) {
        return ['name' => $user->name, 'id' => $user->id];
    }
});
