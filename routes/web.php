<?php

use App\Task;
use App\Project;
use App\Events\TaskCreated;
use App\Events\OrderStatusUpdated;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * OLD ROUTES
 */
Route::get('/', function () {
    return view('welcome');
});

Route::get('/tasks', function() {
    return Task::latest()->pluck('body');
});

Route::post('/tasks', function() {
    $task = Task::forceCreate(request(['body']));

    event((new TaskCreated($task)));
});

/**
 * NEW ROUTES
 */

// Web Routes
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/projects/{project}', function (Project $project) {
    $project->load('tasks');

    return view('welcome', compact('project'));
});

Auth::routes();

// API Routes
Route::get('/api/projects/{project}', function (Project $project) {
    return $project->tasks;
});

Route::post('/api/projects/{project}/tasks', function (Project $project) {
    $task = $project->tasks()->create(request(['body']));

    event(new TaskCreated($task));

    return $task;
});

